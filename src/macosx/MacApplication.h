/*! @file MacApplication.h
 *  @brief TODO: Add the purpose of this module
 *  @author Kyle Weicht
 *  @date 9/13/12
 *  @copyright Copyright (c) 2012 Kyle Weicht. All rights reserved.
 */

#import <Cocoa/Cocoa.h>

#include "application.h"

@interface MacApplication : NSApplication

@end
