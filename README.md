deferred
========

Very much a work in progress. So far:

* Multiple lights
* Basic shadow maps
* Simple component-based entity system
* Normal mapping
* Specular mapping
* Cross-platform (OS X and Windows)
* Procedual terrain (marching cubes algorithm)

